package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.result);

        DownloadTask task = new DownloadTask(imageView);
        task.execute("https://upload.wikimedia.org/wikipedia/commons/4/40/Loosely_sitting_stray_white_cat.JPG");
    }
}
